import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from './dashboard/dashboard.component';
import {SigninComponent} from './users/signin.component';
import {SignupComponent} from './users/signup.component';
import {ResourcesComponent} from './resources/resources.component';
import {ResourcesNewComponent} from './resources/resources-new.component';
import {StrategiesComponent} from './strategies/strategies.component';
import {MartingaleComponent} from './martingale/martingale.component';
import {DocumentationComponent} from './documentation/documentation.component';
import {MartingaleDetailComponent} from './martingale/martingale-detail/martingale-detail.component';

import {AuthGuard} from './guards/index';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'signin',
    component: SigninComponent,
    data: {name: 'Sign in'}
  },
  {
    path: 'signup',
    component: SignupComponent,
    data: {name: 'Sign up'}
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: {name: 'Dashboard'}
  },
  {
    path: 'resources',
    component: ResourcesComponent,
    canActivate: [AuthGuard],
    data: {name: 'Resources'}
  },
  {
    path: 'resources/new',
    component: ResourcesNewComponent,
    canActivate: [AuthGuard],
    data: {name: 'New Resource'}
  },
  {
    path: 'strategies',
    component: StrategiesComponent,
    canActivate: [AuthGuard],
    data: {name: 'Strategies'}
  },
  {
    path: 'strategies/martingale',
    component: MartingaleComponent,
    canActivate: [AuthGuard],
    data: {name: 'Strategies / Martingale'}
  },
  {
    path: 'strategies/martingale/:id',
    component: MartingaleDetailComponent,
    canActivate: [AuthGuard],
    data: {name: 'Strategies / Martingale / Detail'}
  },
  {
    path: 'documentation',
    component: DocumentationComponent,
    canActivate: [AuthGuard],
    data: {name: 'Documentation'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
