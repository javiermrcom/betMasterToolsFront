import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map'
import {AppSettings} from '../app.settings';

@Injectable()
export class AuthenticationService {
  private APIURL = AppSettings.APIURL;
  private logged = new Subject<any>();

  authChange: Subject<string> = new Subject<string>();

  constructor(private http: Http) {
  }

  signin(email: string, password: string) {
    return this.http.post(`${this.APIURL}/signin`, {email: email, password: password})
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        const user = response.json();
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.logged.next(user);
        }

        return user;
      });
  }

  signup(email: string, password: string, displayName: string) {
    return this.http.post(`${this.APIURL}/signup`, {
      email: email,
      password: password,
      displayName: displayName
    })
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        const user = response.json();
        if (user && user.token) {
          user.email = email;
          user.displayName = displayName;
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
  }

  isLoggedObs(): Observable<any> {
    return this.logged.asObservable();
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.logged.next(null);
  }
}