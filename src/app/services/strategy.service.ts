import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {AppSettings} from '../app.settings';

@Injectable()
export class StrategyService {
  private APIURL = AppSettings.APIURL;
  private userId = JSON.parse(localStorage.getItem('currentUser')).id;

  constructor(private http: Http) {
  }

  public getStrategies(strategyName = null): Observable<any> {
    const uri = strategyName ? `${this.APIURL}/strategy?name=${strategyName}` : `${this.APIURL}/strategy`;
    return this.http.get(uri)
      .map(res => res.json().strategies);
  }

  public getStrategyMetrics(strategyId): Observable<any> {
    return this.http.get(`${this.APIURL}/configuration?strategyId=${strategyId}&userId=${this.userId}`)
      .map(res => res.json().configurations);
  }
}
