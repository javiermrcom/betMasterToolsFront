import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {AppSettings} from '../app.settings';

@Injectable()
export class ConfigurationService {
  private APIURL = AppSettings.APIURL;
  private userId = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')).id : null;

  constructor(private http: Http) {
  }

  public getConfiguration(configurationId): Observable<any> {
    return this.http.get(`${this.APIURL}/configuration/${configurationId}`)
      .map(res => res.json().configuration);
  }

  public getConfigurations(): Observable<any> {
    return this.http.get(`${this.APIURL}/configuration/?userId=${this.userId}`)
      .map(res => res.json().configurations);
  }

  public saveConfiguration(configuration): Observable<any> {
    return this.http.post(`${this.APIURL}/configuration`, configuration)
      .map(res => res.json().configuration);
  }

  public updateConfigurationStatus(configurationId, status): Observable<any> {
    return this.http.put(`${this.APIURL}/configuration/${configurationId}`, status)
      .map(res => res.json().configuration);
  }

  public removeConfiguration(configurationId): Observable<any> {
    return this.http.delete(`${this.APIURL}/configuration/${configurationId}`)
      .map(res => res.json())
  }
}
