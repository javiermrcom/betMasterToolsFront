import {async, inject, TestBed} from '@angular/core/testing';
import {AuthenticationService} from './authentication.service';
import {HttpModule} from '@angular/http';

import construct = Reflect.construct;

describe('Service: AuthenticationService', () => {
  const email = Math.random() * 1000000 + '@test.com';
  const password = 'P4ssw0rd%';
  const displayName = 'Test';

  let authenticationService;
  let user;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpModule],
    providers: [AuthenticationService]
  }));

  beforeEach(inject([AuthenticationService], (_authenticationService) => {
    authenticationService = _authenticationService;
  }));

  it('should sign up new user', async(() => {
    authenticationService.logout();

    authenticationService.signup(email, password, displayName)
      .subscribe(_user => {
        user = _user;
        expect(_user.token).toBeDefined();
        expect(_user.email).toEqual(email);
        expect(_user.displayName).toEqual(displayName);

        const LSUser = JSON.parse(localStorage.getItem('currentUser'));

        expect(LSUser).toBeDefined();
        expect(LSUser.email).toEqual(email);
        authenticationService.logout();
      });
  }));

  it('should sign in user', async(() => {
    authenticationService.signin(email, password)
      .subscribe(_user => {
        const LSUser = JSON.parse(localStorage.getItem('currentUser'));
        expect(LSUser).toBeDefined();
        expect(LSUser.email).toEqual(email);
        authenticationService.logout();
      });
  }));

  it('should log out user', async(() => {
    authenticationService.signin(email, password)
      .subscribe(_user => {
        let LSUser = JSON.parse(localStorage.getItem('currentUser'));
        expect(LSUser).toBeDefined();
        expect(LSUser.email).toEqual(email);
        authenticationService.logout();
        LSUser = JSON.parse(localStorage.getItem('currentUser'));
        expect(LSUser).toBe(null);
      });
  }));

  // it('should remove user', async(() => {
  // }));
});
