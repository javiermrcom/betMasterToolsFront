import {async, inject, TestBed} from '@angular/core/testing';
import {SimulateService} from './simulate.service';
import {ConfigurationService} from './configuration.service';
import {HttpModule} from '@angular/http';

import construct = Reflect.construct;
import {LogService} from './log.service';
import {ResultService} from './result.service';

describe('Service: SimulateService', () => {
  const configurationId = '5989fec287f8ab26a3957a0a';
  const moneyEarnerd = 95.8;
  const moneyLost = 40;

  let configurationService;
  let simulateService;
  let configuration;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpModule],
    providers: [
      ConfigurationService,
      SimulateService,
      LogService,
      ResultService
    ]
  }));

  beforeEach(inject([ConfigurationService, SimulateService], (_configurationService, _simulateService) => {
    configurationService = _configurationService;
    simulateService = _simulateService;
  }));

  it('should return configuration', async(() => {
    configurationService.getConfiguration(configurationId)
      .subscribe(_configuration => {
        configuration = _configuration;
        expect(_configuration['_id']).toEqual(configurationId);
      });
  }));

  it('should simulate configuration', async(() => {
    simulateService.simulate(configuration)
      .then(res => {
        expect(res.moneyEarned).toEqual(moneyEarnerd);
        expect(res.moneyLost).toEqual(moneyLost);
        expect(res.configuration.status).toEqual('simulated');
      });
  }));
});
