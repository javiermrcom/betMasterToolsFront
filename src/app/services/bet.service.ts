import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {AppSettings} from '../app.settings';

@Injectable()
export class BetService {
  private APIURL = AppSettings.APIURL;

  constructor(private http: Http) {
  }

  public getBets(): Observable<any> {
    return this.http.get(`${this.APIURL}/bet`)
      .map(res => res.json().bets);
  }

  public saveBets(bets): Observable<any> {
    return this.http.post(`${this.APIURL}/bet`, bets)
      .map(res => res.json().bets);
  }
}
