import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {AppSettings} from '../app.settings';

@Injectable()
export class ResourceService {
  private APIURL = AppSettings.APIURL;
  private userId = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')).id : null;

  constructor(private http: Http) {
  }

  public getResources(): Observable<any> {
    return this.http.get(`${this.APIURL}/resource/?userId=${this.userId}`)
      .map(res => res.json().resources);
  }

  public saveResource(resource): Observable<any> {
    return this.http.post(`${this.APIURL}/resource`, resource)
      .map(res => res.json().resource);
  }

  public updateResource(resourceId, resource): Observable<any> {
    return this.http.put(`${this.APIURL}/resource/${resourceId}`, resource)
      .map(res => res.json().resource);
  }
}