import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {AppSettings} from '../app.settings';

@Injectable()
export class LogService {
  private APIURL = AppSettings.APIURL;
  private userId = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')).id : null;

  constructor(private http: Http) {
  }

  public getLogs(configurationId): Observable<any> {
    let URL = `${this.APIURL}/log/?userId=${this.userId}`;

    if (configurationId) {
      URL += `&configurationId=${configurationId}`;
    }

    return this.http.get(URL)
      .map(res => res.json().logs);
  }

  public saveLogs(logs): Observable<any> {
    return this.http.post(`${this.APIURL}/log`, logs)
      .map(res => res.json().logs);
  }

  public removeLogs(configurationId): Observable<any> {
    return this.http.delete(`${this.APIURL}/log/${configurationId}`)
      .map(res => res.json().logs);
  }
}
