import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {Martingale} from '../models/martingale';
import {AppSettings} from '../app.settings';
import {LogService} from '../services/log.service';
import {ResultService} from '../services/result.service';
import {ConfigurationService} from '../services/configuration.service';
import {Configuration} from '../models/configuration';

@Injectable()
export class SimulateService {
  private APIURL = AppSettings.APIURL;

  team: string;
  timesNotResultMatching: number;
  startBet: number;
  stopBet: number;
  unit: number;
  minBet: number;
  result: string;
  timesBetting: number;
  betQty: number;
  moneyEarned: number;
  moneyLost: number;
  configuration: Configuration;
  logs: Array<any>;

  constructor(private http: Http,
              private logService: LogService,
              private resultService: ResultService,
              private configurationService: ConfigurationService) {

  }

  public simulate(martingale: Martingale): Promise<any> {
    this.timesNotResultMatching = 0;
    this.timesBetting = 0;
    this.moneyEarned = 0;
    this.moneyLost = 0;
    this.configuration = martingale;
    this.logs = [];

    return new Promise((resolve, reject) => {
      // GET BETS BY RESOURCE
      this.getConfigurationBets(martingale)
        .subscribe(res => {
          this.partyHard(martingale, res)
            .then(result => {
              resolve(result);
            })
        });
    });
  }

  private getConfigurationBets(martingale): Observable<any> {
    return this.http.get(`${this.APIURL}/bet/?resourceId=${martingale.resourceId}&team=${martingale.fields.team}`)
      .map(res => res.json().bets);
  }

  private partyHard(configuration, bets): Promise<any> {
    return new Promise((resolve, reject) => {
      this.team = configuration.fields.team;
      this.startBet = Number.parseInt(configuration.fields.startBet);
      this.stopBet = Number.parseInt(configuration.fields.stopBet);
      this.unit = Number.parseFloat(configuration.fields.unit);
      this.minBet = Number.parseFloat(configuration.fields.minBet);
      this.result = configuration.fields.result;

      for (const bet of bets) {
        if (!this.isTimeToBet()) {
          this.simulateMatch(bet);
          continue;
        }

        // BET IF betValue is lt than configuration
        if (this.minBet > this.getBetValue(bet)) {
          this.pushLog(AppSettings.BET_ACTION_WAIT, {
            moneyEarned: this.moneyEarned,
            moneyLost: this.moneyLost
          }, 'BetValue is lt minBet');
          this.simulateMatch(bet);
          continue;
        }

        // DO PARTY
        const result = this.doBet(bet);

        this.stopOrContinue(bet, result);
      }

      // Save Result
      this.saveResult(this.moneyEarned, this.moneyLost);
      // Update Bet Status
      return this.configurationService
        .updateConfigurationStatus(this.configuration['_id'], {
          status: AppSettings.CONFIGURATION_STATUS_SIMULATED
        })
        .subscribe(configurationUpdated => {
          this.sendLogs();
          resolve({
            configuration: configurationUpdated,
            moneyEarned: this.moneyEarned,
            moneyLost: this.moneyLost
          });
        });
    });
  }

  private simulateMatch(bet): void {
    this.pushLog(AppSettings.BET_ACTION_WAIT, {
      moneyEarned: this.moneyEarned,
      moneyLost: this.moneyLost
    });

    let ok = false;
    const matchPlace = this.getMatchPlace(bet);

    switch (this.result) {
      case AppSettings.BET_WIN:
        ok = bet.ftr === matchPlace;
        break;
      case AppSettings.BET_DEAD_MATCH:
        ok = bet.ftr !== AppSettings.BET_DEAD_MATCH;
        break;
      case AppSettings.BET_LOSE:
        ok = bet.ftr !== matchPlace;
        break;
      default:
        alert('error');
    }

    this.timesNotResultMatching = ok ? this.timesNotResultMatching + 1 : 0;
  }

  private doBet(bet): boolean {
    let win = false;
    const matchPlace = bet.homeTeam === this.team ? 'H' : 'A';

    this.updateBetQty();

    switch (this.result) {
      case AppSettings.BET_WIN:
        win = bet.ftr !== matchPlace;
        break;
      case AppSettings.BET_DEAD_MATCH:
        win = bet.ftr === AppSettings.BET_DEAD_MATCH;
        break;
      case AppSettings.BET_LOSE:
        win = bet.ftr === matchPlace;
        break;
      default:
        alert('error');
    }

    this.addTimesBetting();

    this.pushLog(AppSettings.BET_ACTION_BET, {
      moneyEarned: this.moneyEarned,
      moneyLost: this.moneyLost,
      bet: this.betQty
    }, 'Betting for ' + this.getBetValue(bet));

    this.addMoneyLost();

    return win;
  }

  private stopOrContinue(bet, win) {
    const betValue = this.getBetValue(bet);

    if (win) {
      this.updateMoneyEarned(betValue);
      this.resetTimesNotBetting();
      this.resetTimesBetting();

      this.pushLog(AppSettings.BET_ACTION_WIN, {
        moneyEarned: this.moneyEarned,
        moneyLost: this.moneyLost
      });
    } else {
      this.pushLog(AppSettings.BET_ACTION_LOSE, {
        moneyEarned: this.moneyEarned,
        moneyLost: this.moneyLost
      });
      this.checkStopBetting();
    }
  }

  private updateMoneyEarned(betValue) {
    this.moneyEarned += betValue * this.betQty;
  }

  private getBetValue(bet) {
    const matchPlace = this.getMatchPlace(bet);
    return matchPlace === AppSettings.MATCH_HOME ? parseFloat(bet.bh) : parseFloat(bet.ba);
  }

  private getMatchPlace(bet) {
    return bet.homeTeam === this.team ? AppSettings.MATCH_HOME : AppSettings.MATCH_AWAY;
  }

  private addTimesBetting() {
    this.timesBetting++;
  }

  private resetTimesBetting() {
    this.timesBetting = 0;
  }

  private resetTimesNotBetting() {
    this.timesNotResultMatching = 0;
  }

  private addMoneyLost() {
    this.moneyLost += Math.pow(this.unit, this.timesBetting);
  }

  private checkStopBetting() {
    if (this.timesBetting >= this.stopBet) {
      this.timesNotResultMatching = 0;
      this.timesBetting = 0;
    }
  }

  private isTimeToBet() {
    return this.timesNotResultMatching >= this.startBet;
  }

  private updateBetQty() {
    if (this.timesBetting === 0) {
      this.betQty = this.unit;
    } else {
      this.betQty *= 2;
    }
  }

  private pushLog(action, data, description = '') {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.logs.push({
      configurationId: this.configuration['_id'],
      resourceId: this.configuration.resourceId,
      userId: currentUser ? currentUser.id : null,
      action: action,
      data: data,
      description: description
    });
  }

  private sendLogs() {
    this.logService.saveLogs(this.logs)
      .subscribe(res => {
        // console.log(res);
      })
  }

  private saveResult(moneyEarned, moneyLost) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.resultService.saveResult({
      userId: currentUser ? currentUser.id : null,
      configurationId: this.configuration['_id'],
      resourceId: this.configuration.resourceId,
      summary: {
        earned: moneyEarned,
        lost: moneyLost,
        balance: moneyEarned - moneyLost
      }
    }).subscribe(res => {
      // console.log(res);
    })
  }
}
