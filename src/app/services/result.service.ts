import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {Result} from '../models/result';
import {AppSettings} from '../app.settings';

@Injectable()
export class ResultService {
  private APIURL = AppSettings.APIURL;
  private userId = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')).id : null;

  constructor(private http: Http) {
  }

  public saveResult(result): Observable<Result> {
    return this.http.post(`${this.APIURL}/result/?userId=${this.userId}`, result)
      .map(res => res.json().result);
  }

  public getResult(configurationId): Observable<Result> {
    return this.http.get(`${this.APIURL}/result/${configurationId}`)
      .map(res => res.json().result[0]);
  }

  public getResults(): Observable<any> {
    return this.http.get(`${this.APIURL}/result`)
      .map(res => res.json().results);
  }
}
