export class Strategy {
  strategyId: String;
  resourceId: String;
  fields: String; // strategy params
  date: Date;
  userId: String;
}