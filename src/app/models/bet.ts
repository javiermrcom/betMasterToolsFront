export class Bet {
    div: string;
    date: string;
    homeTeam: string;
    awayTeam: string;
    fthg: number;
    ftag: number;
    ftr: string;
    hthg: number;
    htag: number;
    htr: string;
    bh: number;
    bd: number;
    ba: number
}