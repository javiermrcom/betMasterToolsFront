export class Result {
  userId = String;
  configurationId = String;
  resourceId = String;
  summary: {
    earned: Number,
    lost: Number,
    balance: Number
  }
}