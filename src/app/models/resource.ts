export class Resource {
  date: string; // creation date
  count: number; // bets count,
  userId: string;
  name: string;
}
