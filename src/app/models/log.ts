export class Log {
  configurationId = String;
  resourceId = String;
  userId = String;
  action = String;
  data = Object;
  description = String;
}
