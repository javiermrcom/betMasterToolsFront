export class Configuration {
  userId: string;
  strategyId: string;
  resourceId: string;
  name: string;
  fields: Object; // configuration params
  date: string;
  status: string;
}
