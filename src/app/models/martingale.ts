export class Martingale {
  userId: string;
  strategyId: string;
  resourceId: string;
  name: string;
  fields: {
    team
    minBet
    unit
    startBet
    stopBet,
    result
  }; // martingale fields
  date: string;
  status: string;
}
