import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-dashboard-counter',
  templateUrl: './dashboard-counter.component.html',
  styleUrls: ['./dashboard-counter.component.css']
})
export class DashboardCounterComponent implements OnInit {
  @Input()
  private counter: Number;
  @Input()
  public name: String;
  @Input()
  private classes: String;

  private loading = true;
  private currentCounter: number;

  constructor() {
  }

  ngOnInit() {
    this.playCounter();
  }

  private playCounter() {
    this.currentCounter = 0;
    const timing = this.counter < 100 ? 35 : 0;

    setInterval(() => {
      if (this.currentCounter < this.counter) {
        this.currentCounter++;
      } else {
        this.loading = false;
      }
    }, timing);
  }

}
