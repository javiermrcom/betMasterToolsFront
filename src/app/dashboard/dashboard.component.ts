import {Component, OnInit} from '@angular/core';
import {ResourceService} from "../services/resource.service";
import {LogService} from "../services/log.service";
import {ConfigurationService} from "../services/configuration.service";
import {ResultService} from "../services/result.service";
import {StrategyService} from "../services/strategy.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private metrics = {
    bets: 0,
    resources: 0,
    strategies: 0,
    configurations: 0,
    results: 0,
    logs: 0
  };
  // heroes: Hero[] = [];

  constructor(private resourceService: ResourceService,
              private logService: LogService,
              private configurationService: ConfigurationService,
              private resultService: ResultService,
              private strategyService: StrategyService) {
  }

  ngOnInit(): void {
    this.getMetrics();
  }

  private getMetrics() {
    this.resourceService.getResources()
      .subscribe(resources => {
        this.metrics.resources = resources.length;
      });

    this.logService.getLogs(0)
      .subscribe(logs => {
        this.metrics.logs = logs.length;
      });

    this.configurationService.getConfigurations()
      .subscribe(configurations => {
        this.metrics.configurations = configurations.length;
      });

    this.resultService.getResults()
      .subscribe(results => {
        this.metrics.results = results.length;
      });

    this.strategyService.getStrategies()
      .subscribe(strategies => {
        this.metrics.strategies = strategies.length;
      });
  }
}
