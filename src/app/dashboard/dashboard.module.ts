import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardCounterComponent } from './dashboard-counter/dashboard-counter.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DashboardComponent, DashboardCounterComponent]
})
export class DashboardModule { }
