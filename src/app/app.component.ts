import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {AuthenticationService} from './services/authentication.service'
import {TranslateService} from './translate/translate.service'
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'BetMasterTools';
  currentTitle;

  public showNav = false;
  public userName = null;
  public supportedLangs: any[];

  authSubscription: Subscription;

  constructor(private authenticationService: AuthenticationService,
              private _translate: TranslateService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    const user = localStorage.getItem('currentUser');
    this.checkAuth(JSON.parse(user));
    this.authSubscription = this.authenticationService.isLoggedObs()
      .subscribe(userStored => {
        this.checkAuth(userStored);
      });
    this.supportedLangs = [
      {display: 'English', value: 'en'}
    ];

    this.selectLang('en');

    this.router.events.filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        let currentRoute = this.route.root;
        while (currentRoute.children[0] !== undefined) {
          currentRoute = currentRoute.children[0]
        }
        this.currentTitle = currentRoute.snapshot.data.name;
      })
  }

  checkAuth(user) {
    this.showNav = !!user;
    this.userName = user ? user.displayName : '';
  }

  selectLang(lang: string) {
    // set current lang;
    this._translate.use(lang);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.authSubscription.unsubscribe();
  }
}
