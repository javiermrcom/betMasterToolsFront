export class AppSettings {
  public static APIURL = 'http://localhost:3001/api';
  public static CONFIGURATION_STATUS_SIMULATED = 'simulated';
  public static CONFIGURATION_STATUS_NOT_SIMULATED = 'not-simulated';

  public static BET_WIN = 'W';
  public static BET_DEAD_MATCH = 'D';
  public static BET_LOSE = 'L';

  public static MATCH_HOME = 'H';
  public static MATCH_AWAY = 'A';

  public static BET_ACTION_START = 'start';
  public static BET_ACTION_BET = 'bet';
  public static BET_ACTION_WIN = 'win';
  public static BET_ACTION_LOSE = 'lose';
  public static BET_ACTION_WAIT = 'wait';
  public static BET_ACTION_START_BETTING = 'start-bet';
  public static BET_ACTION_STOP_BETTING = 'stop-bet';
  public static BET_ACTION_END = 'end';
}
