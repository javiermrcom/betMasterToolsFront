import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

import {AppComponent} from './app.component';
import {SigninComponent} from './users/signin.component';
import {SignupComponent} from './users/signup.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardCounterComponent} from './dashboard/dashboard-counter/dashboard-counter.component';
import {StrategiesComponent} from './strategies/strategies.component';
import {ResourcesComponent} from './resources/resources.component';
import {ResourcesNewComponent} from './resources/resources-new.component';
import {ResourcesItemComponent} from './resources/resources-item.component';
import {MartingaleComponent} from './martingale/martingale.component';
import {MartingaleItemComponent} from './martingale/martingale-item/martingale-item.component';
import {MartingaleDetailComponent} from './martingale/martingale-detail/martingale-detail.component';
import {DocumentationComponent} from './documentation/documentation.component';
import {NavComponent} from './nav/nav.component';

import {AuthenticationService} from './services/index';
import {AuthGuard} from './guards/index';
import {SimulateService} from './services/simulate.service';
import {LogService} from './services/log.service';
import {ResultService} from './services/result.service';
import {ConfigurationService} from './services/configuration.service';
import {ResourceService} from './services/resource.service';
import {BetService} from './services/bet.service';
import {StrategyService} from './services/strategy.service';

import {AppRoutingModule} from './routing.module';
import {TruncatePipe} from './pipes/truncate-text.pipe';
import {KeysPipe} from './pipes/keys.pipe';
import {TranslatePipe} from './pipes/translate.pipe';
import {TRANSLATION_PROVIDERS, TranslateService} from './translate';
import {BarchartComponent} from './shared/barchart/barchart.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    DashboardComponent,
    DashboardCounterComponent,
    StrategiesComponent,
    ResourcesComponent,
    ResourcesNewComponent,
    ResourcesItemComponent,
    MartingaleComponent,
    MartingaleItemComponent,
    MartingaleDetailComponent,
    DocumentationComponent,
    NavComponent,
    TruncatePipe,
    KeysPipe,
    TranslatePipe,
    BarchartComponent
  ],
  providers: [
    AuthenticationService,
    AuthGuard,
    TRANSLATION_PROVIDERS,
    TranslateService,
    SimulateService,
    LogService,
    ResultService,
    ConfigurationService,
    ResourceService,
    StrategyService,
    BetService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

