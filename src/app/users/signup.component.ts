import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../services/index';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  model: any = {};
  returnUrl: string;
  error: {
    message: string,
    param: string
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.authenticationService.logout();
    this.returnUrl = '/dashboard';
  }

  signup() {
    this.authenticationService.signup(this.model.email, this.model.password, this.model.displayName)
      .subscribe(data => {
          this.authenticationService.signin(this.model.email, this.model.password)
            .subscribe(res => {
              if (this.returnUrl) {
                this.router.navigate([this.returnUrl]);
              }
            });
        },
        error => {
          error = JSON.parse(error._body);
          this.error = {
            message: error.message || '',
            param: error.param || ''
          }
        })
  }

}
