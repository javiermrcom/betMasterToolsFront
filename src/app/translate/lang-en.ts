export const LANG_EN_NAME = 'en';
export const LANG_EN_TRANS = {
  'simulated': 'Simulated',
  'not-simulated': 'Not simulated',
  'W': 'Win',
  'D': 'Dead Match',
  'L': 'Lose'
};
