import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, exponent: number): string {
    if (value != null) {
      const limit = exponent < 1 ? 1 : exponent;
      const trail = (exponent > 1 && value.length < exponent) ? '' : '...';
      return value.substring(0, limit) + trail;
    }
    return value;
  }
}
