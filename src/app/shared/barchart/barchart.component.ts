import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BarchartComponent implements OnInit, OnChanges {

  @ViewChild('chart') private chartContainer: ElementRef;
  @Input() public configurationId: String;
  @Input() private data: Array<any>;
  private chart: any;
  private width: any;
  private widthScale;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.data) {
      this.createChart();
    }
  }

  createChart() {
    if (!this.data[0].value && !this.data[1].value && !this.data[2].value) {
      return;
    }

    const max = this.getMaxAbsValue();
    const $element = this.chartContainer.nativeElement;
    this.width = $element.offsetWidth - 400;

    // SCALE
    this.widthScale = d3.scaleLinear()
      .domain([0, max])
      .range([0, this.width / 2]);

    d3.select('svg').remove();

    // ENTER
    d3.select($element)
      .selectAll('div.bar')
      .data(this.data)
      .enter()
      .append('div')
      .attr('class', 'bar')
      .append('span');

    // UPDATE
    d3.select($element)
      .selectAll('div.bar')
      .data(this.data)
      .attr('class', d => {
        if (this.isPositive(d.value)) {
          return 'bar bar--positive';
        } else {
          return 'bar bar--negative';
        }
      })
      .style('width', '0px')
      .style('margin-left', d => {
        if (this.isPositive(d.value)) {
          return this.width / 2 + 200 + 'px';
        } else {
          return this.width / 2 - this.widthScale(Math.abs(d.value)) + 200 + 'px';
        }
      })
      .transition()
      .ease(d3.easeExp)
      .duration(1000)
      .style('width', d => {
        return this.widthScale(Math.abs(d.value)) + 'px';
      });

    d3.select($element)
      .selectAll('div.bar')
      .data(this.data)
      .select('span')
      .attr('class', 'label')
      .style('opacity', '0')
      .html(d => {
        return `${d.name}<br><span class="value">${Math.abs(d.value).toFixed(1)} €</span>`;
      })
      .transition()
      .duration(1000)
      .delay(250)
      .style('opacity', 1);
  }

  private getMaxAbsValue() {
    const maxValue = Math.max.apply(Math, this.data.map(o => {
      return o.value;
    }));

    const minValue = Math.min.apply(Math, this.data.map(o => {
      return o.value;
    }));

    return Math.max.apply(Math, [maxValue, Math.abs(minValue)]);
  }

  private isPositive(d) {
    return d >= 0;
  }
}
