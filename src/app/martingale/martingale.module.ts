import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MartingaleComponent} from './martingale.component';
import {MartingaleDetailComponent} from './martingale-detail/martingale-detail.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MartingaleComponent, MartingaleDetailComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class MartingaleModule {
}
