import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Martingale} from '../../models/martingale'
import {AppSettings} from '../../app.settings';
import {SimulateService} from '../../services/simulate.service';
import {ConfigurationService} from '../../services/configuration.service';
import {ResultService} from '../../services/result.service';
import {LogService} from '../../services/log.service';

@Component({
  moduleId: module.id,
  selector: 'app-martingale-item',
  templateUrl: './martingale-item.component.html',
  styleUrls: ['./martingale-item.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class MartingaleItemComponent implements OnInit {
  @Input()
  configuration: Martingale;
  @Output()
  configurationDeleted: EventEmitter<any> = new EventEmitter();

  private chartData: Array<any>;

  loading: boolean;

  constructor(private simulateService: SimulateService,
              private configurationService: ConfigurationService,
              private resultService: ResultService,
              private logService: LogService) {
    this.loading = false;
  }

  ngOnInit(): void {
    this.getAndShowResults();
  }

  public simulate() {
    this.loading = true;
    if (this.configuration.status === AppSettings.CONFIGURATION_STATUS_NOT_SIMULATED) {
      this.simulateService.simulate(this.configuration)
        .then(res => {
          // fake time simulator
          this.configuration.status = AppSettings.CONFIGURATION_STATUS_SIMULATED;
          this.loading = false;

          setTimeout(() => {
            this.showResults(res.moneyEarned, res.moneyLost);
          }, 200);
        })
    }
  }

  public remove() {
    this.configurationService.removeConfiguration(this.configuration['_id'])
      .subscribe(configurations => {
        this.configurationDeleted.emit();

        this.logService.removeLogs(this.configuration['_id'])
          .subscribe(logs => {
          });
      });
  }

  private showResults(moneyEarned, moneyLost) {
    this.chartData = [
      {name: 'Money Earned', value: moneyEarned},
      {name: 'Money Lost', value: -moneyLost},
      {name: 'Total', value: (moneyEarned - moneyLost)}
    ];
  }

  private getAndShowResults() {
    if (this.configuration.status !== AppSettings.CONFIGURATION_STATUS_SIMULATED) {
      return;
    }
    this.resultService.getResult(this.configuration['_id'])
      .subscribe(result => {
        if (!result) {
          return;
        }
        this.showResults(result.summary.earned, result.summary.lost);
      });
  }
}
