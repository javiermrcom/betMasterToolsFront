import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {Resource} from '../models/resource'
import {Configuration} from '../models/configuration'
import {AppSettings} from '../app.settings';
import {ResourceService} from '../services/resource.service';
import {ConfigurationService} from '../services/configuration.service';
import {StrategyService} from '../services/strategy.service';

@Component({
  selector: 'app-martingale',
  templateUrl: './martingale.component.html',
  styleUrls: ['./martingale.component.css']
})
export class MartingaleComponent implements OnInit {
  model: any = {};
  showNewConfiguration: boolean;
  loading: boolean;
  teams: Array<string>;
  resources: Array<Resource>;
  configurations: Array<Configuration>;

  constructor(private http: Http,
              private resourceService: ResourceService,
              private configurationService: ConfigurationService,
              private strategyService: StrategyService) {
    this.showNewConfiguration = false;
    this.teams = this.getTeams();
  }

  ngOnInit(): void {
    this.getResources();
    this.getConfigurations();
  }

  newConfiguration() {
    this.showNewConfiguration = true;
    this.loading = false;
  }

  saveConfiguration() {
    this.loading = true;
    const date = new Date();

    this.strategyService.getStrategies('Martingale')
      .subscribe(res => {
        this.configurationService.saveConfiguration({
          userId: JSON.parse(localStorage.getItem('currentUser')).id,
          resourceId: this.model.resource,
          strategyId: res[0]['_id'],
          name: this.model.name,
          date: date.toJSON().split('T')[0],
          fields: {
            team: this.model.team,
            minBet: this.model.minBet,
            unit: this.model.unit,
            startBet: this.model.startBet,
            stopBet: this.model.stopBet,
            result: this.model.result
          },
          status: AppSettings.CONFIGURATION_STATUS_NOT_SIMULATED
        })
          .subscribe(() => {
            this.getConfigurations();
            this.cancel();
          })
      });

  }

  cancel() {
    this.model.resource = null;
    this.model.team = null;
    this.model.name = null;
    this.model.minBet = null;
    this.model.unit = null;
    this.model.startBet = null;
    this.model.stopBet = null;
    this.showNewConfiguration = false;
  }

  getTeams = function () {
    return [
      'La Coruna',
      'Malaga',
      'Granada',
      'Barcelona',
      'Sevilla',
      'Sociedad',
      'Ath Madrid',
      'Sp Gijon',
      'Celta',
      'Valencia',
      'Betis',
      'Espanol',
      'Eibar',
      'Leganes',
      'Osasuna',
      'Real Madrid',
      'Alaves',
      'Ath Bilbao',
      'Las Palmas',
      'Villarreal'
    ]
  };

  getResources() {
    this.resourceService.getResources()
      .subscribe(res => {
        this.resources = res;
      })
  }

  private getConfigurations() {
    this.configurationService.getConfigurations()
      .subscribe(res => {
        this.configurations = this.parseConfigurations(res);
      })
  }

  private parseConfigurations(res) {
    res.map(configuration => {
      this.resources.map(resource => {
        if (configuration.resourceId === resource['_id']) {
          configuration.resourceName = resource.name;
        }
      })
    });

    return res;
  }

  onConfigurationDeleted(configuration) {
    const index = this.configurations.findIndex(conf => (conf['_id'] === configuration['_id']));
    if (index !== -1) {
      this.configurations.splice(index, 1);
    }
  }
}
