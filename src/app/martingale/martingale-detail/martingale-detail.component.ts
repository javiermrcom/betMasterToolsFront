import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LogService} from '../../services/log.service';
import {ResourceService} from '../../services/resource.service';
import {ConfigurationService} from '../../services/configuration.service';
import {Martingale} from '../../models/martingale';
import {Resource} from '../../models/resource';

@Component({
  selector: 'app-martingale-detail',
  templateUrl: './martingale-detail.component.html',
  styleUrls: ['./martingale-detail.component.css']
})
export class MartingaleDetailComponent implements OnInit {
  private id: String;
  private sub: any;
  private logs: Array<any>;
  private configuration: Martingale;
  private isDataAvailable: boolean;
  public resources: Array<Resource>;

  constructor(private route: ActivatedRoute,
              private resourceService: ResourceService,
              private configurationService: ConfigurationService,
              private logService: LogService) {
    this.isDataAvailable = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getResourcesAndConfiguration();
    });
  }

  ngOnInit(): void {
    this.getLogs();
  }

  private getConfiguration() {
    this.configurationService.getConfiguration(this.id)
      .subscribe(configuration => {
        this.configuration = configuration;
        this.resources.map(resource => {
          if (resource['_id'] === configuration.resourceId) {
            this.configuration['resourceName'] = resource.name;
            this.isDataAvailable = true;
            return;
          }
        });
      })
  }

  private getLogs() {
    this.logService.getLogs(this.id)
      .subscribe(logs => this.logs = logs)
  }

  private getResourcesAndConfiguration() {
    this.resourceService.getResources()
      .subscribe(res => {
        this.resources = res;
        this.getConfiguration();
      })
  }
}
