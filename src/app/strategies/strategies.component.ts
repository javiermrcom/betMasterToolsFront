import {Component, OnInit} from '@angular/core';
import {Strategy} from '../models/strategy';
import {StrategyService} from '../services/strategy.service'
import {AppSettings} from '../app.settings';

@Component({
  selector: 'app-strategies',
  templateUrl: 'strategies.component.html',
  styleUrls: ['strategies.component.css']
})
export class StrategiesComponent implements OnInit {

  private strategies: Strategy[];

  constructor(private strategyService: StrategyService) {
    this.strategies = [];
  }

  ngOnInit(): void {
    this.getStrategies();
  }

  private getStrategies() {
    this.strategyService.getStrategies()
      .subscribe(strategies => {
        this.parseStrategies(strategies);
      });
  }

  private parseStrategies(strategies) {
    strategies.map(strategy => {
      this.strategyService.getStrategyMetrics(strategy['_id'])
        .subscribe(configurations => {
          let stored = 0;
          let simulated = 0;

          configurations.map(configuration => {
            if (configuration.status === AppSettings.CONFIGURATION_STATUS_SIMULATED) {
              simulated++;
            }
            stored++;
          });

          strategy.metrics = {
            stored,
            simulated
          }
        });

      this.strategies = strategies;
    });
  }
}
