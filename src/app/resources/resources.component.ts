import {Component, OnInit} from '@angular/core';
import {ResourceService} from '../services/resource.service';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html'
})
export class ResourcesComponent implements OnInit {
  private showNewResource: boolean;
  private resources;

  constructor(private resourceService: ResourceService) {
  }

  ngOnInit(): void {
    this.getResources();
  }

  public newResource() {
    this.showNewResource = true;
  }

  private getResources() {
    return this.resourceService.getResources()
      .subscribe(resources => {
        this.resources = resources;
      });
  }
}
