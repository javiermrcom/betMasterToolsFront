import {Component, Input, OnInit} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';
import {ResourceService} from '../services/resource.service';
import {BetService} from '../services/bet.service';

@Component({
  selector: 'app-resources-new',
  templateUrl: './resources-new.component.html'
})
export class ResourcesNewComponent implements OnInit {
  @Input() private resources: Array<any>;
  @Input() private showNewResource: any;

  private resourceId;

  showInputFile;
  name;

  constructor(private resourceService: ResourceService,
              private betService: BetService) {
    this.showInputFile = false;
  }

  ngOnInit(): void {
  }

  changeResourceListener($event): void {
    this.readThis($event.target);
  }

  changeNameListener(): void {
    this.showInputFile = !!this.name;
  }

  readThis(inputValue: any) {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();
    const fileType = inputValue.parentElement.id;

    myReader.onloadend = (e) => {
      // you can perform an action with readed data here
      this.parseDataAndSend(myReader.result);
    };

    myReader.readAsText(file);
  }

  parseDataAndSend(data) {
    const date = new Date();

    this.resourceService.saveResource({
      date: date.toJSON().split('T')[0],
      userId: JSON.parse(localStorage.getItem('currentUser')).id,
      name: this.name
    })
      .subscribe(resource => {
        this.resourceId = resource['_id'];
        this.parseData(data);
      });
  }

  parseData(data) {
    const headersToStore = {
      Div: 'div',
      Date: 'date',
      HomeTeam: 'homeTeam',
      AwayTeam: 'awayTeam',
      FTHG: 'fthg',
      FTAG: 'ftag',
      FTR: 'ftr',
      HTHG: 'hthg',
      HTAG: 'htag',
      HTR: 'htr',
      B365H: 'bh',
      B365D: 'bd',
      B365A: 'ba'
    };

    const lines = data.split('\n');
    const result = [];
    const headers = lines[0].split(',');

    for (let i = 1; i < lines.length; i++) {
      const obj = {};
      const currentLine = lines[i].split(',');

      Object.keys(headersToStore).map(key => {
        if (key === 'Date' && currentLine[headers.indexOf(key)]) {
          let date = currentLine[headers.indexOf(key)].split('/');
          date = '20' + date[2] + '/' + date[1] + '/' + date[0];
          currentLine[headers.indexOf(key)] = date;
        }

        obj[headersToStore[key]] = currentLine[headers.indexOf(key)];
      });

      obj['resourceId'] = this.resourceId;
      result.push(obj);
    }

    data = result;

    this.uploadData(data);
  }

  uploadData(bets) {
    const total = bets.length;

    this.betService.saveBets(bets)
      .subscribe(bets => {
        console.log(bets);
      });

    // Update Resource.count
    this.resourceService.updateResource(this.resourceId, {
      count: total
    })
      .subscribe(resource => {
        this.resetForm();
        this.addResource(resource, total);
      });
  }

  private resetForm() {
    this.name = null;
    this.showNewResource = false;
  }

  private addResource(resource, total) {
    resource.count = total;
    resource.UIClass = 'new';
    this.resources.unshift(resource);
  }
}
