import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ResourcesComponent} from './resources.component';
import {BrowserModule} from '@angular/platform-browser';
import {NgUploaderModule} from 'ngx-uploader';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    NgUploaderModule
  ],
  declarations: [
    ResourcesComponent
  ]
})
export class ResourcesModule {

}
