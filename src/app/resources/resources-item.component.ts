import {Component, Input, OnInit} from '@angular/core';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Component({
  selector: 'app-resources-item',
  templateUrl: './resources-item.component.html',
  styleUrls: ['./resources-item.component.css']
})
export class ResourcesItemComponent implements OnInit {
  @Input() private resource: any;

  constructor() {
  }

  ngOnInit(): void {

  }
}
