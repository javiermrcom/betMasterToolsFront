import { BetMasterToolsPage } from './app.po';

describe('bet-master-tools App', () => {
  let page: BetMasterToolsPage;

  beforeEach(() => {
    page = new BetMasterToolsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
